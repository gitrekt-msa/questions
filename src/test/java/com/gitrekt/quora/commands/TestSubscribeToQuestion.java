package com.gitrekt.quora.commands;

import com.gitrekt.quora.commands.handlers.SubscribeToQuestionCommand;
import com.gitrekt.quora.commands.handlers.UnsubscribeToQuestionCommand;
import com.gitrekt.quora.exceptions.ServerException;
import org.junit.Test;

import java.sql.SQLException;
import java.util.HashMap;

public class TestSubscribeToQuestion {

  @Test
  public void testSubscribe() throws ServerException, SQLException {

    HashMap<String, Object> args = new HashMap<>();

    args.put("user_id", "7b33e4fc-1a41-4661-a4d9-563fc21cd89e");
    args.put("question_id", "7b33e4fc-1a41-4661-a4d9-563fc21ce90f");

    SubscribeToQuestionCommand cmd = new SubscribeToQuestionCommand(args);

    cmd.execute();
  }


  @Test
  public void testUnsubscribe() throws ServerException, SQLException {

    HashMap<String, Object> args = new HashMap<>();

    args.put("user_id", "7b33e4fc-1a41-4661-a4d9-563fc21cd89e");
    args.put("question_id", "7b33e4fc-1a41-4661-a4d9-563fc21ce90f");

    UnsubscribeToQuestionCommand cmd = new UnsubscribeToQuestionCommand(args);

    cmd.execute();
  }
}
