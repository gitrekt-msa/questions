package com.gitrekt.quora.commands;

import com.gitrekt.quora.commands.handlers.DeleteQuestionCommand;
import com.gitrekt.quora.exceptions.ServerException;
import org.junit.Test;

import java.sql.SQLException;
import java.util.HashMap;

public class TestDeleteQuestionCommand {
  String questionId = "5f7d5b2c-ea5a-4c52-bb11-af70e16f3648";
  String userId = "0d50fcd3-eed6-4774-8ac0-43c0c2b72b0d";

  @Test
  public void testDeleteQuestion() throws ServerException, SQLException {
    HashMap<String, Object> args = new HashMap<>();
    args.put("question_id", questionId);
    args.put("user_id", userId);

    DeleteQuestionCommand cmd = new DeleteQuestionCommand(args);

    cmd.execute();
  }

  @Test(expected = SQLException.class)
  public void testDeleteQuestionWithoutPermission() throws ServerException, SQLException {
    HashMap<String, Object> args = new HashMap<>();
    args.put("question_id", questionId);
    args.put("user_id", "7b33e4fc-1a41-4661-a4d9-563fc21cd89e");

    DeleteQuestionCommand cmd = new DeleteQuestionCommand(args);

    cmd.execute();
  }
}
