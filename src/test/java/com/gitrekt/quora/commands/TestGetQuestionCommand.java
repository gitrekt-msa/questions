package com.gitrekt.quora.commands;

import com.gitrekt.quora.commands.handlers.GetQuestionCommand;
import com.gitrekt.quora.exceptions.NotFoundException;
import com.gitrekt.quora.exceptions.ServerException;
import com.google.gson.JsonObject;
import org.junit.Test;

import java.sql.SQLException;
import java.util.HashMap;

import static org.junit.Assert.assertTrue;

public class TestGetQuestionCommand {
  String questionId = "5f7d5b2c-ea5a-4c52-bb11-af70e16f3649";

  @Test
  public void testGetQuestion() throws ServerException, SQLException {
    HashMap<String, Object> args = new HashMap<>();

    args.put("question_id", questionId);

    GetQuestionCommand cmd = new GetQuestionCommand(args);

    JsonObject result = (JsonObject) cmd.execute();

    System.out.println(result.toString());

    assertTrue(result.get("title").getAsString(), result.get("title").getAsString().contains("time"));
  }


  @Test(expected = NotFoundException.class)
  public void testGetQuestionThatDoesNotExist() throws ServerException, SQLException {
    HashMap<String, Object> args = new HashMap<>();

    args.put("question_id", "5f7d5b2c-ea5a-4c52-bb11-af70e16f3641");

    GetQuestionCommand cmd = new GetQuestionCommand(args);

    JsonObject result = (JsonObject) cmd.execute();
  }
}
