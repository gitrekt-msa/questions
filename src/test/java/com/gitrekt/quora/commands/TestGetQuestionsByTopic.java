package com.gitrekt.quora.commands;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.sql.SQLException;
import java.util.HashMap;

import com.gitrekt.quora.commands.handlers.ViewQuestionsByTopicCommand;
import com.gitrekt.quora.exceptions.AuthenticationException;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.junit.Test;

public class TestGetQuestionsByTopic {

  String message = "Hello";

  @Test
  public void testInvalidUUID() {
    HashMap<String, Object> attributes = new HashMap<>();
    attributes.put("topic_id", "1");
    attributes.put("page_number", "0");
    boolean failed = false;
    ViewQuestionsByTopicCommand cmd = new ViewQuestionsByTopicCommand(attributes);
    try {
      cmd.execute();
    } catch (IllegalArgumentException | SQLException | BadRequestException | AuthenticationException ex) {
      failed = true;
    }
    assertEquals("Request should fail due to invalid user id", true, failed);
  }

  @Test
  public void testNoQuestionsForThisTopicQuestions() {
    HashMap<String, Object> attributes = new HashMap<>();
    attributes.put("page_number", "0");
    attributes.put("topic_id", "75e5128f-5edd-4b60-97e1-bbc7a36990d6");
    ViewQuestionsByTopicCommand cmd = new ViewQuestionsByTopicCommand(attributes);
    JsonObject result = null;
    try {
      result = (JsonObject) cmd.execute();
    } catch (IllegalArgumentException | SQLException | BadRequestException | AuthenticationException ex) {
      ex.printStackTrace();
      fail("Test should not have thrown exception");
    }
    JsonObject body = (JsonObject) result.get("body");
    JsonArray responseArray = (JsonArray)body.get("questions");
    boolean success = responseArray.size()==0;
    assertEquals("Response should be empty"+result,true,success);
  }
  @Test
  public void testQuestionsForTopics() {
    HashMap<String, Object> attributes = new HashMap<>();
    attributes.put("page_number", "0");
    attributes.put("topic_id", "aacebd04-5ceb-11e9-8647-d663bd873d93");
    ViewQuestionsByTopicCommand cmd = new ViewQuestionsByTopicCommand(attributes);
    JsonObject result = null;
    try {
      result = (JsonObject) cmd.execute();
    } catch (IllegalArgumentException | SQLException | BadRequestException | AuthenticationException ex) {
      ex.printStackTrace();
      fail("Test should not have thrown exception");
    }
    JsonObject body = (JsonObject) result.get("body");
    JsonArray responseArray = (JsonArray)body.get("questions");

    boolean success = responseArray.size()!=0;
    assertEquals("Response should not be empty"+result,true,success);
  }
}