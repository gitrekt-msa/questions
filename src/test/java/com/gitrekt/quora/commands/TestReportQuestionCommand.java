package com.gitrekt.quora.commands;

import com.gitrekt.quora.commands.handlers.ReportQuestionsCommand;
import com.gitrekt.quora.exceptions.BadRequestException;
import org.junit.Test;

import java.sql.SQLException;
import java.util.HashMap;

public class TestReportQuestionCommand {

  @Test
  public void testReportQuestion() throws BadRequestException, SQLException {
    HashMap<String, Object> args = new HashMap<>();

    args.put("user_id", "7b33e4fc-1a41-4661-a4d9-563fc21cd89e");
    args.put("question_id", "5f7d5b2c-ea5a-4c52-bb11-af70e16f3648");

    ReportQuestionsCommand cmd = new ReportQuestionsCommand(args);

    cmd.execute();
  }
}
