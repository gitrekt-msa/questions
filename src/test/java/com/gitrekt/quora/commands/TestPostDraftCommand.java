package com.gitrekt.quora.commands;

import com.gitrekt.quora.commands.handlers.PostDraftCommand;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.gitrekt.quora.exceptions.ServerException;
import org.junit.Test;

import java.sql.SQLException;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;

public class TestPostDraftCommand {
  String userId = "0d50fcd3-eed6-4774-8ac0-41c0c2b72b0d";

  @Test
  public void testFullDraft() {
    HashMap<String, Object> attributes = new HashMap<>();
    attributes.put("user_id", userId);
    attributes.put("title", "Test");
    attributes.put("body", "Test body");
    PostDraftCommand cmd = new PostDraftCommand(attributes);

    boolean success = true;
    String msg = "";
    try {
      cmd.execute();
    } catch (Exception | Error ex) {
      success = false;
      msg = ex.getMessage();
    }
    assertEquals("No missing arguments should not fail " + msg, success, true);
  }

  @Test
  public void testMissingTitleDraft() {
    HashMap<String, Object> attributes = new HashMap<>();
    attributes.put("user_id", userId);
    attributes.put("body", "Test body");
    PostDraftCommand cmd = new PostDraftCommand(attributes);

    boolean success = true;
    String msg = "";
    try {
      cmd.execute();
    } catch (Exception | Error ex) {
      success = false;
      msg = ex.getMessage();
    }
    assertEquals("Missing title should not fail " + msg, success, true);
  }

  @Test
  public void testMissingBodyDraft() {
    HashMap<String, Object> attributes = new HashMap<>();
    attributes.put("user_id", userId);
    attributes.put("title", "Test");
    PostDraftCommand cmd = new PostDraftCommand(attributes);

    boolean success = true;
    String msg = "";
    try {
      cmd.execute();
    } catch (Exception | Error ex) {
      success = false;
      msg = ex.getMessage();
    }
    assertEquals("Missing body should not fail " + msg, success, true);
  }

  @Test(expected = BadRequestException.class)
  public void testMissingUserId() throws ServerException, SQLException {
    HashMap<String, Object> attributes = new HashMap<>();

    attributes.put("title", "Test");
    attributes.put("body", "Test body");

    PostDraftCommand cmd = new PostDraftCommand(attributes);

    cmd.execute();
  }
}
