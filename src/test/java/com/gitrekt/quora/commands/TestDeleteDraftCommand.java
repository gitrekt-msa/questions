package com.gitrekt.quora.commands;

import com.gitrekt.quora.commands.handlers.DeleteDraftCommand;
import com.gitrekt.quora.exceptions.ServerException;
import org.junit.Test;

import java.sql.SQLException;
import java.util.HashMap;

public class TestDeleteDraftCommand {
    String userId = "0d50fcd3-eed6-4774-8ac0-42c0c2b72b0d";

    @Test
    public void testDeleteDraft() throws ServerException, SQLException {
        HashMap<String, Object> args = new HashMap<>();
        args.put("user_id", userId);

        DeleteDraftCommand cmd = new DeleteDraftCommand(args);

        cmd.execute();
    }
}
