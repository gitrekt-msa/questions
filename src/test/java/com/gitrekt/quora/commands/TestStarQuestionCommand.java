package com.gitrekt.quora.commands;

import com.gitrekt.quora.commands.handlers.StarQuestionCommand;
import com.gitrekt.quora.exceptions.BadRequestException;
import org.junit.Test;

import java.sql.SQLException;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;

public class TestStarQuestionCommand {
    @Test
    public void testInvalidUUID() {
        HashMap<String, Object> attributes = new HashMap<>();
        attributes.put("user_id", "1");
        attributes.put("question_id", "1");
        boolean failed = false;
        StarQuestionCommand cmd = new StarQuestionCommand(attributes);
        try {
            cmd.execute();
        } catch (IllegalArgumentException | SQLException | BadRequestException ex) {
            failed = true;
        }
        assertEquals(true, failed);
    }

    @Test
    public void testQuestionUUIDNotExist() {
        HashMap<String, Object> attributes = new HashMap<>();
        attributes.put("user_id", "0a6c1cec-6767-4c16-bf5f-f8cd7819da38");
        attributes.put("question_id", "845c7a26-7a01-4c3a-9df4-bd3336d3ce74");
        boolean failed = false;
        StarQuestionCommand cmd = new StarQuestionCommand(attributes);
        try {
            cmd.execute();
        } catch (IllegalArgumentException | SQLException | BadRequestException ex) {
            failed = true;
        }
        assertEquals(true, failed);
    }
}
