package com.gitrekt.quora.commands;

import com.gitrekt.quora.commands.handlers.AddTopicCommand;
import com.gitrekt.quora.exceptions.ServerException;
import org.junit.Test;

import java.sql.SQLException;
import java.util.HashMap;

public class TestAddTopicCommand {

  @Test
  public void testAddTopic() throws ServerException, SQLException {

    HashMap<String, Object> args = new HashMap<>();

    args.put("name", randomStr());

    AddTopicCommand cmd = new AddTopicCommand(args);

    cmd.execute();
  }

  String randomStr(){
    char[] arr = new char[10];

    for(int i = 0;i<10;i++)
      arr[i] = (char)('a'+(int)(Math.random()*26));

    return new String(arr);
  }
}
