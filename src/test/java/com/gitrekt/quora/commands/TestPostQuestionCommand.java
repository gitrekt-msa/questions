package com.gitrekt.quora.commands;

import com.gitrekt.quora.commands.handlers.PostQuestionCommand;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.gitrekt.quora.exceptions.ServerException;
import org.junit.Test;

import java.sql.SQLException;
import java.util.HashMap;

import static org.junit.Assert.assertFalse;

public class TestPostQuestionCommand {

  String userId = "7b33e4fc-1a41-4661-a4d9-563fc21cd89e";
  String questionId = "7b33e4fc-1a41-4661-a4d9-563fc21ce90f";

  String userId2 = "0d50fcd3-eed6-4774-8ac0-43c0c2b72b0d";

  @Test
  public void testValidPost() {
    HashMap<String, Object> attributes = new HashMap<>();
    attributes.put("user_id", userId);
    attributes.put("title", "Test");
    attributes.put("body", "Test body");
    attributes.put("media", "{\"paths\":[\"test/test\"]}");
    boolean failed = false;
    PostQuestionCommand postQuestionCmd = new PostQuestionCommand(attributes);
    String msg = "";
    try {
      postQuestionCmd.execute();
    } catch (Exception ex) {
      failed = true;
      msg = ex.getMessage();
    } catch (Error err){
      failed = true;
      msg = err.getMessage();
    }
    assertFalse(msg, failed);
  }


  @Test(expected = BadRequestException.class)
  public void testMissingUserId() throws ServerException, SQLException {
    HashMap<String, Object> attributes = new HashMap<>();

    attributes.put("title", "Test");
    attributes.put("body", "Test body");
    attributes.put("media", "{\"paths\":[\"test/test\"]}");

    PostQuestionCommand postQuestionCmd = new PostQuestionCommand(attributes);

    postQuestionCmd.execute();
  }


  @Test
  public void testEditQuestion() throws ServerException, SQLException {
    HashMap<String, Object> attributes = new HashMap<>();

    attributes.put("question_id", questionId);
    attributes.put("user_id", userId);
    attributes.put("title", "Test");
    attributes.put("body", "Test body");
    attributes.put("media", "{\"paths\":[\"test/test\"]}");

    PostQuestionCommand postQuestionCmd = new PostQuestionCommand(attributes);

    postQuestionCmd.execute();
  }


  @Test(expected = SQLException.class)
  public void testEditQuestionWithoutPermission() throws ServerException, SQLException {
    HashMap<String, Object> attributes = new HashMap<>();

    attributes.put("question_id", questionId);
    attributes.put("user_id", userId2);
    attributes.put("title", "Test");
    attributes.put("body", "Test body");
    attributes.put("media", "{\"paths\":[\"test/test\"]}");

    PostQuestionCommand postQuestionCmd = new PostQuestionCommand(attributes);

    postQuestionCmd.execute();
  }
}
