package com.gitrekt.quora.commands;

import com.gitrekt.quora.commands.handlers.AddQuestionTopicCommand;
import com.gitrekt.quora.commands.handlers.DeleteQuestionTopicCommand;
import com.gitrekt.quora.exceptions.ServerException;
import org.junit.Test;

import java.sql.SQLException;
import java.util.HashMap;

public class TestAddAndDeleteTopicToQuestion {

  @Test
  public void testAddTopicToQuestion() throws ServerException, SQLException {

    HashMap<String, Object> args = new HashMap<>();

    args.put("question_id","5f7d5b2c-ea5a-4c52-bb11-af70e16f3649");
    args.put("topic_id","cf9b40b7-9f18-4d0f-9aac-a982bc0c0de2");

    AddQuestionTopicCommand cmd = new AddQuestionTopicCommand(args);

    cmd.execute();
  }


  @Test
  public void testDeleteTopicFromQuestion() throws ServerException, SQLException {

    HashMap<String, Object> args = new HashMap<>();

    args.put("question_id","5f7d5b2c-ea5a-4c52-bb11-af70e16f3649");
    args.put("topic_id","cf9b40b7-9f18-4d0f-9aac-a982bc0c0de2");

    DeleteQuestionTopicCommand cmd = new DeleteQuestionTopicCommand(args);

    cmd.execute();
  }

}
