package com.gitrekt.quora.commands;

import com.gitrekt.quora.commands.handlers.SubscribeToTopicCommand;
import com.gitrekt.quora.commands.handlers.UnsubscribeToTopicCommand;
import com.gitrekt.quora.exceptions.ServerException;
import org.junit.Test;

import java.sql.SQLException;
import java.util.HashMap;

public class TestSubscribeToTopic {

  @Test
  public void testSubscribe() throws ServerException, SQLException {

    HashMap<String, Object> args = new HashMap<>();

    args.put("user_id", "7b33e4fc-1a41-4661-a4d9-563fc21cd89e");
    args.put("topic_id", "cf9b40b7-9f18-4d0f-9aac-a982bc0c0de2");

    SubscribeToTopicCommand cmd = new SubscribeToTopicCommand(args);

    cmd.execute();
  }


  @Test
  public void testUnsubscribe() throws ServerException, SQLException {

    HashMap<String, Object> args = new HashMap<>();

    args.put("user_id", "7b33e4fc-1a41-4661-a4d9-563fc21cd89e");
    args.put("topic_id", "cf9b40b7-9f18-4d0f-9aac-a982bc0c0de2");

    UnsubscribeToTopicCommand cmd = new UnsubscribeToTopicCommand(args);

    cmd.execute();
  }
}
