package com.gitrekt.quora.commands;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import com.gitrekt.quora.commands.handlers.GetBookmarkedQuestionsCommand;

import java.sql.SQLException;
import java.util.HashMap;

import com.gitrekt.quora.exceptions.BadRequestException;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.junit.Test;

public class TestGetBookmarkedQuestionsCommand {

  String message = "Hello";

  @Test
  public void testInvalidUUID() {
    HashMap<String, Object> attributes = new HashMap<>();
    attributes.put("user_id", "1");
    attributes.put("page_number", 0);
    boolean failed = false;
    GetBookmarkedQuestionsCommand cmd = new GetBookmarkedQuestionsCommand(attributes);
    try {
      cmd.execute();
    } catch (IllegalArgumentException | SQLException | BadRequestException ex) {
      failed = true;
    }
    assertEquals("Request should fail due to invalid user id", true, failed);
  }

  @Test
  public void testNoBookmarkedQuestions() {
    HashMap<String, Object> attributes = new HashMap<>();
    attributes.put("page_number", "0");
    attributes.put("user_id", "0d50fcd3-eed6-4774-8ac0-42c0c2b72b0d");
    GetBookmarkedQuestionsCommand cmd = new GetBookmarkedQuestionsCommand(attributes);
    JsonObject result = null;
    try {
      result = (JsonObject) cmd.execute();
    } catch (IllegalArgumentException | SQLException | BadRequestException ex) {
      ex.printStackTrace();
      fail("Test should not have thrown exception");
    }
    JsonObject body = (JsonObject) result.get("body");
    JsonArray responseArray = (JsonArray)body.get("questions");

    boolean success = responseArray.size()==0;
    assertEquals("Response should be empty"+result,true,success);
  }
  @Test
  public void testBookmarkedQuestions() {
    HashMap<String, Object> attributes = new HashMap<>();
    attributes.put("page_number", "0");
    attributes.put("user_id", "7b33e4fc-1a41-4661-a4d9-563fc21cd89e");
    GetBookmarkedQuestionsCommand cmd = new GetBookmarkedQuestionsCommand(attributes);
    JsonObject result = null;
    try {
      result = (JsonObject) cmd.execute();
    } catch (IllegalArgumentException | SQLException | BadRequestException ex) {
      ex.printStackTrace();
      fail("Test should not have thrown exception");
    }
    JsonObject body = (JsonObject) result.get("body");
    JsonArray responseArray = (JsonArray)body.get("questions");

    boolean success = responseArray.size()!=0;
    assertEquals("Response should not be empty"+result,true,success);
  }
}
