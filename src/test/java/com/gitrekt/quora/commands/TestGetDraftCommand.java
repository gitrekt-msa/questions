package com.gitrekt.quora.commands;

import com.gitrekt.quora.commands.handlers.GetDraftCommand;
import com.gitrekt.quora.exceptions.ServerException;
import com.google.gson.JsonObject;
import org.junit.Test;

import java.sql.SQLException;
import java.util.HashMap;

import static org.junit.Assert.assertTrue;

public class TestGetDraftCommand {
    String userId = "11ea2793-3cc6-4b01-8819-b2b8df3aa166";

    @Test
    public void testGetDraft() throws ServerException, SQLException {
        HashMap<String, Object> args = new HashMap<>();

        args.put("user_id", userId);

        GetDraftCommand cmd = new GetDraftCommand(args);

        JsonObject result = (JsonObject) cmd.execute();

        assertTrue(result.get("title").getAsString(), result.get("title").getAsString().contains("this is the test draft"));
    }
}
