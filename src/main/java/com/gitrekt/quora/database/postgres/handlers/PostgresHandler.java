package com.gitrekt.quora.database.postgres.handlers;

import com.gitrekt.quora.database.postgres.PostgresConnection;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

public abstract class PostgresHandler<T> {

  protected final String tableName;
  protected Class<T> mapper;

  /**
   * Postgres Handler Constructor.
   *
   * @param table Table name.
   * @param mapper Class to map rows to.
   */
  public PostgresHandler(String table, Class<T> mapper) {
    tableName = table;
    this.mapper = mapper;
  }

  protected ResultSet call(String sql, int[] types, Connection connection, Object... params)
      throws SQLException {
    // DbUtils does not support calling procedures?
    CallableStatement callableStatement = connection.prepareCall(sql);
    for (int i = 0; i < types.length; i++) {
      callableStatement.setObject(i + 1, params[i], types[i]);
    }
    boolean containsResults = callableStatement.execute();
    if (containsResults) {
      return callableStatement.getResultSet();
    }
    return null;
  }

  protected void insert(String sql, Connection connection, Object... params) throws SQLException {
    CallableStatement callableStatement = connection.prepareCall(sql);
    for (int i = 0; i < params.length; i++) {
      if (i == 0) {
        callableStatement.setObject(i + 1, params[i], Types.OTHER);
      } else {
        callableStatement.setString(i + 1, (String) params[i]);
      }
    }
    callableStatement.execute();

    callableStatement.close();
  }

  protected void delete(String sql, Connection connection, Object... params) throws SQLException {
    CallableStatement callableStatement = connection.prepareCall(sql);
    for (int i = 0; i < params.length; i++) {
      callableStatement.setObject(i + 1, params[i], Types.OTHER);
    }
    callableStatement.execute();
    callableStatement.close();
  }
}
