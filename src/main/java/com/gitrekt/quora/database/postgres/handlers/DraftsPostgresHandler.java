package com.gitrekt.quora.database.postgres.handlers;

import com.gitrekt.quora.database.postgres.PostgresConnection;
import com.gitrekt.quora.models.Draft;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.UUID;

public class DraftsPostgresHandler extends PostgresHandler<Draft> {

  public DraftsPostgresHandler() {
    super("Drafts", Draft.class);
  }

  /**
   * Adds a new draft to the database, or updates the current draft if the user already has one.
   *
   * @param params the inputs to the sql procedure.
   * @throws SQLException thrown when database error occurs
   */
  public void insertDraft(Object... params) throws SQLException {
    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      super.insert("CALL Insert_Draft(?, ?, ?)", connection, params);
    }
  }

  /**
   * Deletes the user's current draft if it exists.
   *
   * @param params the inputs to the sql procedure.
   * @throws SQLException thrown when database error occurs
   */
  public void deleteDraft(Object... params) throws SQLException {
    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      super.delete("CALL Delete_Draft(?)", connection, params);
    }
  }

  /**
   * Returns the user's current draft if it exists.
   *
   * @param userId the id of the user requesting the draft.
   * @throws SQLException thrown when database error occurs
   */
  public Draft getDraft(String userId) throws SQLException {
    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      String sql = "SELECT * FROM Get_Draft_By_User_Id(?)";
      int[] types = {Types.OTHER};
      ResultSet query = super.call(sql, types, connection, UUID.fromString(userId));
      Draft draft = new Draft();
      while (query.next()) {
        draft.setUserId(query.getString("user_id"));
        draft.setTitle(query.getString("title"));
        draft.setBody(query.getString("body"));
      }
      query.close();
      return draft;
    }
  }

}
