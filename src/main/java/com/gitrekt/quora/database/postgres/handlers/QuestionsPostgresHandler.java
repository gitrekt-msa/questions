package com.gitrekt.quora.database.postgres.handlers;

import com.gitrekt.quora.database.postgres.PostgresConnection;
import com.gitrekt.quora.models.Question;
import com.google.gson.JsonParser;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

public class QuestionsPostgresHandler extends PostgresHandler<Question> {

  public QuestionsPostgresHandler() {
    super("Questions", Question.class);
  }

  /**
   * Adds a new question to the database.
   *
   * @param params the inputs to the sql procedure.
   * @throws SQLException thrown when database error occurs
   */
  public void postQuestion(Object... params) throws SQLException {
    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      int paramSize = params.length;
      int[] types = new int[paramSize];
      for (int idx = 0; idx < paramSize; idx++) {
        types[idx] = Types.OTHER;
      }
      // Type of title and body
      types[2] = Types.VARCHAR;
      types[3] = Types.VARCHAR;
      String sql = "CALL Insert_Question(?, ?, ?, ?, ?)";
      super.call(sql, types, connection, params);
    }
  }

  /**
   * sets the deleted_at attribute of a question in the database.
   *
   * @param params the inputs to the sql procedure.
   * @throws SQLException thrown when database error occurs
   */
  public void deleteQuestion(Object... params) throws SQLException {
    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      String sql = "CALL Delete_Question(?, ?)";
      int paramSize = params.length;
      int[] types = new int[paramSize];
      for (int idx = 0; idx < paramSize; idx++) {
        types[idx] = Types.OTHER;
      }
      super.call(sql, types, connection, params);
    }
  }

  /**
   * Adds a Report to a Question to the database.
   *
   * @param params the inputs to the sql procedure.
   * @throws SQLException thrown when database error occurs
   */
  public void postQuestionReport(Object... params) throws SQLException {
    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      String sql = "CALL Insert_Report(?, ?)";
      int paramSize = params.length;
      int[] types = new int[paramSize];
      for (int idx = 0; idx < paramSize; idx++) {
        types[idx] = Types.OTHER;
      }
      super.call(sql, types, connection, params);
    }
  }

  /**
   * Adds an entry to the topic_question table.
   *
   * @param params the inputs to the sql procedure.
   * @throws SQLException thrown when database error occurs
   */
  public void insertQuestionTopic(Object... params) throws SQLException {
    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      String sql = "CALL Insert_Question_Topic(?, ?)";
      int paramSize = params.length;
      int[] types = new int[paramSize];
      for (int idx = 0; idx < paramSize; idx++) {
        types[idx] = Types.OTHER;
      }
      super.call(sql, types, connection, params);
    }
  }

  /**
   * Deletes an entry from the topic_question table.
   *
   * @param params the inputs to the sql procedure.
   * @throws SQLException thrown when database error occurs
   */
  public void deleteQuestionTopic(Object... params) throws SQLException {
    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      String sql = "CALL Delete_Question_Topic(?, ?)";
      int paramSize = params.length;
      int[] types = new int[paramSize];
      for (int idx = 0; idx < paramSize; idx++) {
        types[idx] = Types.OTHER;
      }
      super.call(sql, types, connection, params);
    }
  }

  /**
   * Adds an entry to the topics table.
   *
   * @param params the inputs to the sql procedure.
   * @throws SQLException thrown when database error occurs
   */
  public void insertTopic(Object... params) throws SQLException {
    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      String sql = "CALL Insert_Topic(?, ?)";
      int paramSize = params.length;
      int[] types = new int[paramSize];
      for (int idx = 0; idx < paramSize; idx++) {
        types[idx] = Types.OTHER;
      }
      // Topic name type
      types[1] = Types.VARCHAR;
      super.call(sql, types, connection, params);
    }
  }

  /**
   * Adds an entry to the user_subscribe_topic table.
   *
   * @param params the inputs to the sql procedure.
   * @throws SQLException thrown when database error occurs
   */
  public void insertUserSubscribeTopic(Object... params) throws SQLException {
    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      String sql = "CALL Insert_User_Subscribe_Topic(?, ?)";
      int paramSize = params.length;
      int[] types = new int[paramSize];
      for (int idx = 0; idx < paramSize; idx++) {
        types[idx] = Types.OTHER;
      }
      super.call(sql, types, connection, params);
    }
  }

  /**
   * Removes an entry from the user_subscribe_topic table.
   *
   * @param params the inputs to the sql procedure.
   * @throws SQLException thrown when database error occurs
   */
  public void deleteUserSubscribeTopic(Object... params) throws SQLException {
    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      String sql = "CALL Delete_User_Subscribe_Topic(?, ?)";
      int paramSize = params.length;
      int[] types = new int[paramSize];
      for (int idx = 0; idx < paramSize; idx++) {
        types[idx] = Types.OTHER;
      }
      super.call(sql, types, connection, params);
    }
  }

  /**
   * Adds an entry to the user_subscribe_to_question table.
   *
   * @param params the inputs to the sql procedure.
   * @throws SQLException thrown when database error occurs
   */
  public void insertUserSubscribeQuestion(Object... params) throws SQLException {
    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      String sql = "CALL Insert_User_Subscription_Question(?, ?)";
      int paramSize = params.length;
      int[] types = new int[paramSize];
      for (int idx = 0; idx < paramSize; idx++) {
        types[idx] = Types.OTHER;
      }
      super.call(sql, types, connection, params);
    }
  }

  /**
   * Removes an entry from the user_subscribe_to_question table.
   *
   * @param params the inputs to the sql procedure.
   * @throws SQLException thrown when database error occurs
   */
  public void deleteUserSubscribeQuestion(Object... params) throws SQLException {
    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      String sql = "CALL Delete_User_Subscription_Question(?, ?)";
      int paramSize = params.length;
      int[] types = new int[paramSize];
      for (int idx = 0; idx < paramSize; idx++) {
        types[idx] = Types.OTHER;
      }
      super.call(sql, types, connection, params);
    }
  }

  /**
   * Adds an entry to the user_bookmark_question table.
   *
   * @param params the inputs to the sql procedure.
   * @throws SQLException thrown when database error occurs
   */
  public void insertUserBookmarkQuestion(Object... params) throws SQLException {
    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      String sql = "CALL Insert_User_Bookmark_Question(?, ?)";
      int paramSize = params.length;
      int[] types = new int[paramSize];
      for (int idx = 0; idx < paramSize; idx++) {
        types[idx] = Types.OTHER;
      }
      super.call(sql, types, connection, params);
    }
  }

  /**
   * Adds an entry to the user_star_question table.
   *
   * @param params the inputs to the sql procedure.
   * @throws SQLException thrown when database error occurs
   */
  public void insertUserStarQuestion(Object... params) throws SQLException {
    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      String sql = "CALL Insert_User_Star_Question(?, ?)";
      int paramSize = params.length;
      int[] types = new int[paramSize];
      for (int idx = 0; idx < paramSize; idx++) {
        types[idx] = Types.OTHER;
      }

      super.call(sql, types, connection, params);
    }
  }

  /**
   * Retrieves all the bookmarked questions belonging to the user whose id is sent as a parameter.
   *
   * @param params the inputs to the sql function.
   * @return the resulting table of questions.
   * @throws SQLException thrown when database error occurs.
   */
  public List<Question> getBookmarkedQuestions(Object... params) throws SQLException {
    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      String sql = "SELECT * from Get_Bookmarked_Questions(?,?)";
      int paramSize = params.length;
      int[] types = new int[paramSize];
      for (int idx = 0; idx < paramSize; idx++) {
        types[idx] = Types.OTHER;
      }
      // type for page number
      types[0] = Types.INTEGER;
      ResultSet result = super.call(sql, types, connection, params);

      List<Question> questions = new ArrayList<>();
      while (result.next()) {
        Question question = new Question();

        question.setId(result.getString("question_id"));
        question.setUserId(result.getString("user_id"));
        question.setPollId(result.getString("poll_id"));
        question.setTitle(result.getString("title"));
        question.setBody(result.getString("body"));
        question.setCreatedAt(result.getTimestamp("created_at"));
        question.setUpdatedAt(result.getTimestamp("updated_at"));
        question.setUpvotes(result.getInt("upvotes"));
        question.setSubscribers(result.getInt("subscribers"));
        question.setMedia(new JsonParser().parse(result.getString("media")).getAsJsonObject());

        questions.add(question);
      }
      result.close();
      return questions;
    }
  }

  /**
   * Retrieves the question with a specified id.
   *
   * @param questionId the id of the question to be retrieved.
   * @return the resulting question.
   * @throws SQLException thrown when database error occurs.
   */
  public Question getQuestion(String questionId) throws SQLException {
    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      String sql = "SELECT * FROM Get_Question_By_Id(?)";
      int[] types = {Types.OTHER};
      ResultSet query = super.call(sql, types, connection, questionId);
      Question question = new Question();
      while (query.next()) {
        question.setId(query.getString("question_id"));
        question.setUserId(query.getString("user_id"));
        question.setPollId(query.getString("poll_id"));
        question.setTitle(query.getString("title"));
        question.setBody(query.getString("body"));
        question.setMedia(new JsonParser().parse(query.getString("media")).getAsJsonObject());
        question.setCreatedAt(query.getTimestamp("created_at"));
        question.setUpdatedAt(query.getTimestamp("updated_at"));
      }

      query.close();
      return question;
    }
  }

  /**
   * Gets the questions that are asked about a specific topic.
   *
   * @param params the inputs to the sql function.
   * @return the resulting table of questions.
   * @throws SQLException thrown when database error occurs.
   */
  public List<Question> viewQuestionsByTopic(Object... params) throws SQLException {
    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      String sql = "SELECT * from Get_Questions_By_Topic(?,?)";
      int paramSize = params.length;
      int[] types = new int[paramSize];
      for (int idx = 0; idx < paramSize; idx++) {
        types[idx] = Types.OTHER;
      }
      // type for page number
      types[0] = Types.INTEGER;

      ResultSet result = super.call(sql, types, connection, params);

      List<Question> returnQuestions = new ArrayList<>();
      while (result.next()) {

        Question currentQuestion = new Question();
        currentQuestion.setId(result.getString("question_id"));
        currentQuestion.setUserId(result.getString("user_id"));
        currentQuestion.setPollId(result.getString("poll_id"));
        currentQuestion.setTitle(result.getString("title"));
        currentQuestion.setBody(result.getString("body"));
        currentQuestion.setCreatedAt(result.getTimestamp("created_at"));
        currentQuestion.setUpdatedAt(result.getTimestamp("updated_at"));
        currentQuestion.setUpvotes(result.getInt("upvotes"));
        currentQuestion.setSubscribers(result.getInt("subscribers"));
        currentQuestion.setMedia(
            new JsonParser().parse(result.getString("media")).getAsJsonObject());
        returnQuestions.add(currentQuestion);
      }
      result.close();
      return returnQuestions;
    }
  }
}
