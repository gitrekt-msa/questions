package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.QuestionsPostgresHandler;
import com.gitrekt.quora.exceptions.ServerException;
import com.google.gson.JsonObject;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

public class SubscribeToTopicCommand extends Command {
  static String[] argsNames = {"topic_id", "user_id"};

  public SubscribeToTopicCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public Object execute() throws SQLException, ServerException {
    checkArguments(argsNames);

    UUID topicId = UUID.fromString((String) args.get("topic_id"));
    UUID userId = UUID.fromString((String) args.get("user_id"));

    setPostgresHandler(new QuestionsPostgresHandler());

    ((QuestionsPostgresHandler)postgresHandler).insertUserSubscribeTopic(userId, topicId);

    JsonObject res = new JsonObject();
    res.addProperty("status_code",200);
    res.addProperty("message","Subscribed to topic successfully");

    return res;
  }
}
