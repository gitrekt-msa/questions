package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.QuestionsPostgresHandler;
import com.gitrekt.quora.exceptions.NotFoundException;
import com.gitrekt.quora.exceptions.ServerException;
import com.gitrekt.quora.models.Question;
import com.google.gson.JsonObject;

import java.sql.SQLException;
import java.util.HashMap;

public class GetQuestionCommand extends Command {
  public GetQuestionCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public Object execute() throws SQLException, ServerException {
    checkArguments(new String[] {"question_id"});

    String questionId = (String) args.get("question_id");

    setPostgresHandler(new QuestionsPostgresHandler());

    Question question =
            ((QuestionsPostgresHandler)postgresHandler).getQuestion(questionId);

    JsonObject resBody = new JsonObject();
    if (question.getId() == null) {
      throw new NotFoundException();
    }
    resBody.addProperty("question_id", questionId);
    resBody.addProperty("user_id", question.getUserId());
    resBody.addProperty("poll_id", question.getPollId());
    resBody.addProperty("title", question.getTitle());
    resBody.addProperty("body", question.getBody());
    resBody.add("media", question.getMedia());
    resBody.addProperty("upvotes", question.getUpvotes());
    resBody.addProperty("subscribers", question.getSubscribers());
    resBody.addProperty("created_at", question.getCreatedAt().toString());
    resBody.addProperty("updated_at", question.getUpdatedAt().toString());

    return resBody;
  }
}
