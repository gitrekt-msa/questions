package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.QuestionsPostgresHandler;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.google.gson.JsonObject;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

public class UnsubscribeToQuestionCommand extends Command {
  static String[] argsNames = {"question_id", "user_id"};

  public UnsubscribeToQuestionCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public Object execute() throws SQLException, BadRequestException {
    checkArguments(argsNames);

    UUID questionId = UUID.fromString((String) args.get("question_id"));
    UUID userId = UUID.fromString((String) args.get("user_id"));

    setPostgresHandler(new QuestionsPostgresHandler());
    ((QuestionsPostgresHandler)postgresHandler).deleteUserSubscribeQuestion(userId, questionId);

    JsonObject res = new JsonObject();
    res.addProperty("status_code",200);
    res.addProperty("message","Unsubscribed from question successfully");

    return res;
  }
}
