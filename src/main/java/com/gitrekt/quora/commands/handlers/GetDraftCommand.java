package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.DraftsPostgresHandler;
import com.gitrekt.quora.exceptions.NotFoundException;
import com.gitrekt.quora.exceptions.ServerException;
import com.gitrekt.quora.models.Draft;
import com.google.gson.JsonObject;

import java.sql.SQLException;
import java.util.HashMap;

public class GetDraftCommand extends Command {
  public GetDraftCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public Object execute() throws SQLException, ServerException {
    checkArguments(new String[] {"user_id"});

    String userId = (String) args.get("user_id");

    setPostgresHandler(new DraftsPostgresHandler());

    Draft draft = ((DraftsPostgresHandler) postgresHandler).getDraft(userId);

    JsonObject resBody = new JsonObject();
    if (draft.getUserId() == null) {
      throw new NotFoundException();
    }
    resBody.addProperty("user_id", draft.getUserId());
    resBody.addProperty("title", draft.getTitle());
    resBody.addProperty("body", draft.getBody());

    return resBody;
  }
}
