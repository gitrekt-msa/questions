package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.QuestionsPostgresHandler;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.gitrekt.quora.models.Question;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/*
    This command allows users to get all the questions that he had previously bookmarked.
*/

public class GetBookmarkedQuestionsCommand extends Command {

  private static final String[] argumentNames = new String[] {"page_number", "user_id"};

  public GetBookmarkedQuestionsCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public Object execute() throws SQLException, BadRequestException {
    checkArguments(argumentNames);
    UUID userId = UUID.fromString((String) args.get("user_id"));
    int pageNumber = Integer.parseInt((String) args.get("page_number"));
    setPostgresHandler(new QuestionsPostgresHandler());

    List<Question> result =
            ((QuestionsPostgresHandler)postgresHandler).getBookmarkedQuestions(pageNumber, userId);
    JsonObject res = new JsonObject();
    res.addProperty("status_code", "200");
    JsonObject resBody = new JsonObject();
    resBody.addProperty("message", "Success");
    JsonArray questionsArray = new JsonArray();

    for (Question question : result) {
      JsonObject parsedQuestion = question.toJson();
      questionsArray.add(parsedQuestion);
    }

    resBody.add("questions", questionsArray);
    res.add("body", resBody);

    return res;
  }
}
