package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.QuestionsPostgresHandler;
import com.gitrekt.quora.exceptions.ServerException;
import com.google.gson.JsonObject;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

public class AddTopicCommand extends Command {
  static String[] argsNames = {"name"};

  public AddTopicCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public Object execute() throws SQLException, ServerException {
    checkArguments(argsNames);
    String name = (String) args.get("name");
    UUID topicId = UUID.randomUUID();

    setPostgresHandler(new QuestionsPostgresHandler());
    ((QuestionsPostgresHandler)postgresHandler).insertTopic(topicId, name);

    JsonObject res = new JsonObject();
    res.addProperty("message","Topic added successfully");

    return res;
  }
}
