package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.QuestionsPostgresHandler;
import com.gitrekt.quora.exceptions.ServerException;
import com.google.gson.JsonObject;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

public class DeleteQuestionCommand extends Command {

  public DeleteQuestionCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public Object execute() throws SQLException, ServerException {
    checkArguments(new String[]{"question_id", "user_id"});

    setPostgresHandler(new QuestionsPostgresHandler());

    UUID questionId = UUID.fromString((String) args.get("question_id"));
    UUID userId = UUID.fromString((String) args.get("user_id"));

    ((QuestionsPostgresHandler)postgresHandler).deleteQuestion(questionId, userId);

    JsonObject res = new JsonObject();
    res.addProperty("message","Question deleted successfully");

    return res;
  }
}
