package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.QuestionsPostgresHandler;
import com.gitrekt.quora.exceptions.AuthenticationException;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.gitrekt.quora.models.Question;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;


public class ViewQuestionsByTopicCommand extends Command {
  static String[] argsNames = {"topic_id", "page_number"};

  public ViewQuestionsByTopicCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public Object execute() throws SQLException, BadRequestException, AuthenticationException {
    checkArguments(argsNames);
    setPostgresHandler(new QuestionsPostgresHandler());
    UUID topicId = UUID.fromString((String) args.get("topic_id"));
    String pageNumber = (String) args.get("page_number");

    List<Question> questionsList =
            ((QuestionsPostgresHandler)postgresHandler).viewQuestionsByTopic(pageNumber, topicId);

    JsonObject res = new JsonObject();
    res.addProperty("status_code", "200");
    JsonArray questionsArray = new JsonArray();

    for (Question question : questionsList) {
      JsonObject parsedQuestion = question.toJson();
      questionsArray.add(parsedQuestion);
    }

    JsonObject resBody = new JsonObject();
    resBody.add("questions", questionsArray);
    res.add("body", resBody);

    return res;
  }
}
