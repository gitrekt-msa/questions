package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.DraftsPostgresHandler;
import com.gitrekt.quora.exceptions.BadRequestException;

import com.google.gson.JsonObject;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;


public class PostDraftCommand extends Command {
  private static final String [] requiredArgs = {"user_id"};

  public PostDraftCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public Object execute() throws BadRequestException, SQLException {
    checkArguments(requiredArgs);

    String userId = (String) args.get("user_id");
    String title = (String) ((args.containsKey("title")) ? args.get("title") : "");
    String body = (String) ((args.containsKey("body")) ? args.get("body") : "");

    setPostgresHandler(new DraftsPostgresHandler());
    ((DraftsPostgresHandler)postgresHandler).insertDraft(UUID.fromString(userId), title, body);

    JsonObject res = new JsonObject();
    res.addProperty("status_code", 200);
    JsonObject resBody = new JsonObject();
    resBody.addProperty("user_id",userId);
    resBody.addProperty("title",title);
    resBody.addProperty("body",body);
    res.add("body", resBody);

    return res;
  }
}
