package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.DraftsPostgresHandler;
import com.gitrekt.quora.exceptions.ServerException;
import com.google.gson.JsonObject;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

public class DeleteDraftCommand extends Command {

  public DeleteDraftCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public Object execute() throws SQLException, ServerException {
    checkArguments(new String[] {"user_id"});

    setPostgresHandler(new DraftsPostgresHandler());

    UUID userId = UUID.fromString((String) args.get("user_id"));

    ((DraftsPostgresHandler) postgresHandler).deleteDraft(userId);

    JsonObject res = new JsonObject();
    res.addProperty("message", "Draft deleted successfully");

    return res;
  }
}
