package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.QuestionsPostgresHandler;
import com.gitrekt.quora.exceptions.ServerException;
import com.google.gson.JsonObject;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

public class AddQuestionTopicCommand extends Command {
  static String[] argsNames = {"topic_id","question_id"};

  public AddQuestionTopicCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public Object execute() throws SQLException, ServerException {
    checkArguments(argsNames);
    UUID topicId = UUID.fromString((String) args.get("topic_id"));
    UUID questionId = UUID.fromString((String) args.get("question_id"));

    setPostgresHandler(new QuestionsPostgresHandler());
    ((QuestionsPostgresHandler)postgresHandler).insertQuestionTopic(topicId, questionId);

    JsonObject res = new JsonObject();
    res.addProperty("message","Topic added to question successfully");

    return res;
  }
}
