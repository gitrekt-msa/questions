package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.QuestionsPostgresHandler;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.google.gson.JsonObject;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

/*
    This command allows users to UPVOTE a question.

*/

public class StarQuestionCommand extends Command {

  private static final String[] argumentNames = new String[] {"question_id", "user_id"};

  public StarQuestionCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public Object execute() throws SQLException, BadRequestException {
    checkArguments(argumentNames);
    UUID userId = UUID.fromString((String) args.get("user_id"));
    UUID questionId = UUID.fromString((String) args.get("question_id"));

    setPostgresHandler(new QuestionsPostgresHandler());
    ((QuestionsPostgresHandler)postgresHandler).insertUserStarQuestion(userId, questionId);

    JsonObject res = new JsonObject();
    res.addProperty("status_code", "200");
    JsonObject resBody = new JsonObject();
    resBody.addProperty(
        "message", "The question with id" + questionId.toString() + "was successfully starred");
    res.add("body", resBody);

    return res;
  }
}
