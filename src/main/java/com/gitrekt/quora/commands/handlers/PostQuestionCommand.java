package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.QuestionsPostgresHandler;

import com.gitrekt.quora.exceptions.ServerException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

/** This command adds a new question with the given arguments. */
public class PostQuestionCommand extends Command {
  static String[] argsNames = {"user_id", "title"};

  public PostQuestionCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public Object execute() throws SQLException, ServerException {

    checkArguments(argsNames);

    setPostgresHandler(new QuestionsPostgresHandler());

    String userId = (String) args.get("user_id");
    String title = (String) args.get("title");
    String body = (String) args.get("body");
    String media = (String)args.get("media");

    UUID questionId =
        args.containsKey("question_id")
            ? UUID.fromString((String) args.get("question_id"))
            : UUID.randomUUID();

    ((QuestionsPostgresHandler)postgresHandler)
            .postQuestion(questionId, userId, title, body, media);

    JsonObject resBody = new JsonObject();
    resBody.addProperty("question_id", questionId.toString());
    resBody.addProperty("user_id", userId);
    resBody.addProperty("title", title);
    resBody.addProperty("body", body);
    resBody.add("media", new JsonParser().parse(media == null ? "{}" : media).getAsJsonObject());
    resBody.addProperty("upvotes", 0);
    resBody.addProperty("subscribers", 0);

    JsonObject res = new JsonObject();
    res.add("body", resBody);

    return res;
  }
}
