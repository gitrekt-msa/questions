package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.QuestionsPostgresHandler;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.google.gson.JsonObject;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

public class ReportQuestionsCommand extends Command {
  static String[] argsNames = { "user_id", "question_id" };

  public ReportQuestionsCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public Object execute() throws SQLException, BadRequestException {
    checkArguments(argsNames);

    setPostgresHandler(new QuestionsPostgresHandler());
    UUID userId = UUID.fromString((String) (args.get("user_id")));
    UUID questionId = UUID.fromString((String) (args.get("question_id")));

    ((QuestionsPostgresHandler)postgresHandler).postQuestionReport(userId, questionId);
    JsonObject resBody = new JsonObject();
    resBody.addProperty("message", "report added successfully");

    return resBody;
  }
}
