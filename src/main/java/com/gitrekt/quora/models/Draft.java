package com.gitrekt.quora.models;

public class Draft {

  private String userId;
  private String title;
  private String body;

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  @Override
  public String toString() {
    return "User{" + "id='" + userId + '\''
            + ", title='" + title + '\'' + ", body='" + body + '\'' + '}';
  }
}
